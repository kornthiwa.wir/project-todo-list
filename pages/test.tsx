import ChildComponent from '@/components/tess';
import React from 'react';

const ParentComponent = () => {
  const handleResult1 = (result: any) => {
    console.log('Result from Child 1:', result);
  };

  const handleResult2 = (result: any) => {
    console.log('Result from Child 2:', result);
  };

  return (
    <div>
      {/* ส่ง callback และฟังก์ชันการประมวลผลไปยัง ChildComponent แบบที่ 1 */}
      <ChildComponent onResult={(result:any)=>console.log(result)} processData={(value: number) => value * 2} />

      {/* ส่ง callback และฟังก์ชันการประมวลผลไปยัง ChildComponent แบบที่ 2 */}
      <ChildComponent onResult={handleResult2} processData={(value: number) => value + 10} />
    </div>
  );
};

export default ParentComponent;
